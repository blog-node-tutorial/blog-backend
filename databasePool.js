const { Pool } = require('pg');
const databaseConfiguration = require('./secrets/databaseConfig');

const pool = new Pool(databaseConfiguration);

module.exports = pool;