#!/bin/bash

export PGPASSWORD='node_password'

echo "Configuring Database"

dropdb -U node_user blogdb;
createdb -U node_user blogdb;

psql -U node_user blogdb < ./bin/sql/account.sql
psql -U node_user blogdb < ./bin/sql/blog.sql
psql -U node_user blogdb < ./bin/sql/accountBlog.sql

echo "Database Configured"