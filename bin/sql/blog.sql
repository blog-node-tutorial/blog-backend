CREATE TABLE blog(
    id              SERIAL PRIMARY KEY,
    body            VARCHAR(128),
    title           VARCHAR(32)
)