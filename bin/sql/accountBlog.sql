CREATE TABLE accountBlog(
    "accountId" INTEGER REFERENCES account(id),
    "blogId" INTEGER REFERENCES blog(id),
    PRIMARY KEY ("accountId", "blogId")
); 