// const uuid = require('uuid/v4');
const { v4: uuid } = require('uuid');
const SEPARATOR = '|';
const { hash } = require('./helper');

class Session {
    constructor({ username }){
        this.username = username;
        this.id = uuid();
    }

    toString(){
        const { username, id } = this ;
        return Session.sessionString({ username, id });
    }


    static parse(sessionString){
        
        const sessionData = sessionString.split(SEPARATOR);

        return {
            username: sessionData[0],
            id: sessionData[1],
            sessionHash: sessionData[2]
        };
    }

    static verify(sessionString){

        const { username, id, sessionHash } = Session.parse(sessionString);
        
        console.log("username : ",username);
        console.log("sessionHash : ",id);
        console.log("sessionHash : ",sessionHash);
        
        const accountData = Session.accountData({ username, id });  
        console.log("Hash Account Data : ",hash(accountData));
        console.log("sessionHash : ",sessionHash);
        return hash(accountData) === sessionHash
    }



    static accountData({ username, id }){
        return `${username}${SEPARATOR}${id}`;
    }

    static sessionString({ username, id }){
        const accountData = Session.accountData({ username, id })
        return `${accountData}${SEPARATOR}${hash(accountData)}`;
    }
}

module.exports = Session;