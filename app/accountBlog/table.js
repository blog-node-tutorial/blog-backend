const pool = require('../../databasePool');

class AccountBlogTable {
    
    static storeAccountBlog({ accountId, blogId }){
        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO accountBlog("accountId", "blogId") VALUES ($1, $2)`,
                [accountId, blogId],
                (error, response) => {
                    if(error) return reject(error);

                    resolve();
                })
        })
    };

    static getAllAccountBlog(){

        console.log("GEt All Accounts");


        return new Promise((resolve, reject) => {
            pool.query(`SELECT "username", "title", "body",account.id as accountid, blog.id as blogid 
            from accountBlog 
            left join account on accountBlog."accountId"=account.id 
            right join blog on accountBlog."blogId"=blog.id`,
                [],
                (error, response) => {
                    if(error) return reject(error);
                    
                    const blogs = response.rows;
                    resolve({blogs});
                })
        });
    }

}

module.exports = AccountBlogTable;