const pool = require('../../databasePool');

class Blog{

    static storeBlog({ blog }){
        
        const { body, title } = blog;
        return new Promise((resolve, reject ) => {

            pool.query(`INSERT INTO blog ( body, title) VALUES ($1, $2) RETURNING id`,
            [body, title],
            ( error, response ) => {
                if(error) return reject(error);

                const blogId = response.rows[0].id;

                resolve({blogId});
            });

        });
    }

    static getBlogs(){
        return new Promise((resolve, reject) => {
            pool.query(
                'SELECT * FROM blog',
                [],
                (error, response) => {
                    if(error) return reject(error);7

                    resolve({ blogs: response.rows });
                }
            )
        })

    }

}

module.exports = Blog;