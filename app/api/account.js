const { Router } = require('express');
const router = new Router();
const { hash } = require('../account/helper');
const { setSession, authenticatedAccount } = require('./helper');
const AccountTable = require('../account/table');
const Session = require('../account/session');

router.post('/signup', (req, res, next) => {
    const { username, password } = req.body;

    AccountTable.getAccount({ username })
        .then(({ account}) => {
            if(!account){
                return AccountTable.storeAccount({ username, password });
            }
            else{
                const error = new Error();
                error.message = 'Username already exist';
                error.statusCode = 409;
                throw error;
            }
        })
        .then(() => {
            return setSession({ username, res });
        })
        .then((message) => {
            res.json({ 
                message
            });
        })
        .catch(error => next(error));
});

router.post('/login', (req, res, next) => {

    const { username, password } = req.body;
    console.log(req.body);
    console.log('username : ',username);
    console.log('password : ',password);

    AccountTable.getAccount({ username })
        .then(({ account}) => {

            if(account && password.localeCompare(account.password) ){
                
                const { sessionId } = account;
                return setSession({ username, res, sessionId });
            }
            else{
                const error = new Error();
                error.message = 'Incorrect Username / Password';
                error.statusCode = 409;
                throw error;
            }
        })
        .then((message) => {
            res.json({ 
                message
            });
        })
        .catch(error => next(error));
});


router.get('/logout', (req, res, next) => {

    const { username } = Session.parse(req.cookies.sessionString);
    AccountTable.updateSessionId({
        sessionId: null,
        username
    }).then(() => {
        res.clearCookie('sessionString');
        res.json({ message: 'Successful Logout '});
        
    }).catch(error => next(error));

});


router.get('/authenticated', (req, res, next) => {

    const { sessionString } = req.cookies;

    authenticatedAccount({ sessionString })
        .then(({ authenticated }) => res.json({ authenticated }))
        .catch(error => next(error));

})

module.exports = router;