const { Router } = require('express');
const router = new Router();
const blogTable = require('../blog/table');
const { authenticatedAccount } = require('./helper');
const AccountBlogTable = require('../accountBlog/table');

router.post('/new', (req, res, next) => {

    let accountId;

    const { body, title } = req.body;
    const blog = { body, title };
    const { sessionString } = req.cookies;

    authenticatedAccount({ sessionString })
    .then(({account}) => {

        accountId = account.id;
        console.log('Account Id : ',account.id)
        return blogTable.storeBlog({blog})
    })
    .then(({blogId}) => {

        blog.blogId = blogId;

        return AccountBlogTable.storeAccountBlog({ accountId, blogId })
    })
    .then(() => {
        res.json({
            message: "added successfully",
            blog: blog
        });
    })
    .catch(error => next(error));
});

router.get('/home', (req, res, next) => {
    
    
    AccountBlogTable.getAllAccountBlog()
    .then(({blogs}) => {
        
        res.json({
            blogs
        })
    })
    .catch(error => next(error));

    // blogTable.getBlogs()
    // .then(({blogs}) => {
    //     res.json({
    //         blogs
    //     })
    // })
    // .catch(error => next(error));

});

module.exports = router;