const AccountTable = require('../account/table');
const Session = require('../account/session');

const setSession = ({ username, res, sessionId }) => {
    return new Promise(( resolve, reject) => {
        
        let session, sessionString;

        if(sessionId){

            sessionString = Session.sessionString({ username, id: sessionId });
            setSessionCookie({ sessionString, res});
            resolve({ message : 'session restored '});

        }
        else{
            session = new Session({ username });
            sessionString = session.toString();

            AccountTable.updateSessionId({
                sessionId: session.id,
                username: username
            })
            .then(() => {
                setSessionCookie({ sessionString, res });
                resolve({ message: 'session created' });
            })
            .catch(error => reject(error));
        }

    });
}

const setSessionCookie = ({ sessionString, res }) => {

    res.cookie('sessionString', sessionString, {
        expire: Date.now() + 120000,
        httpOnly: true,
        // secure: true //use with https
    });
};



const authenticatedAccount = ({ sessionString }) => {

    console.log("sessionString : ",sessionString);

    return new Promise((resolve, reject) => {

        if(!sessionString || !Session.verify(sessionString)){
            
            console.log("Authentication Error");
            
            const error = new Error();
            error.message = "Unauthorized User"
            error.statusCode = 401;
            
            return reject(error);
    
        }else{
            
            const {username, id} = Session.parse(sessionString);
    
            AccountTable.getAccount(({ username }))
                .then(({ account }) => {
                    
                    const authenticated = account.sessionId === id;
                    resolve({ account, authenticated });
    
                })
                .catch(error => {
                    reject(error)
                });
        }
    })
}


module.exports = { setSession, authenticatedAccount }