//Libraries
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');

//Routers
const accountRouter = require('./api/account');
const blogRouter = require('./api/blog');


//Middlewares
const app = express();
app.use(cors({
    origin: 'http://localhost:1234',
    credentials: true
 }));
app.use(bodyParser.json());
app.use(cookieParser());



//Routes
app.use('/account', accountRouter);
app.use('/blog', blogRouter);



//handling server errors
//next(error) calls this function
app.use((err, req, res, next) => {

    //if status code is not provided set default to 500
    const statusCode = err.statusCode || 500

    res.status(statusCode).json({
        type:"error",
        message: err.message
    })

});

module.exports = app;