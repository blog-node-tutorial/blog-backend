module.exports = {
    user: 'node_user',
    host: 'localhost',
    database: 'blogdb',
    password: 'node_password',
    port: 5432
}